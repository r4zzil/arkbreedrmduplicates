import sys
import traceback
from xml.etree import ElementTree


class XmlReader:
    def __init__(self, path):
        self.path = path
        self.xml = ElementTree.parse(path)
        root = self.xml.getroot()
        self.creatures_node = root.find('creatures')
        self.deleted = 0

    def delete_duplicate_guid(self):
        known = []
        self.deleted = 0
        for creature in self.creatures_node.findall('Creature'):
            guid = creature.find('guid').text
            if guid in known:
                self._delete(creature)
            else:
                known.append(guid)
        print 'deleted {} duplicate creatures'.format(self.deleted)

    def _delete(self, creature):
        self.creatures_node.remove(creature)
        self.deleted += 1

    def save(self, path=None):
        if path is None:
            path = self.path
        self.xml.write(path)


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print 'no database file passed'
    else:
        try:
            xml = XmlReader(sys.argv[1])
            xml.delete_duplicate_guid()
            xml.save()
        except Exception:
            print 'the passed string is not a database file'
            print traceback.print_exc()
