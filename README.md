# ArkBreedRmDuplicates

A simple python2.7 script which removes duplicate creature entries from 
[ARKStatsExtractors](https://github.com/cadon/ARKStatsExtractor) 
creature database. 


## Version
0.1.0


## Author
Razzil
 

## Limitation

This version deletes creatures with the same guid. Leaving only one 
creature for each.
 
 
## Usage

Run the python script and pass the path of the Database file as first 
parameter.
``` 
python RmDuplicates.py <pathToDatabaseFile>
```


